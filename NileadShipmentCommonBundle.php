<?php

/**
 * Created by Rubikin Team.
 * ========================
 * Date: 10/25/2014
 * Time: 1:12 PM
 * Author: ThinhNguyen
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class NileadShipmentCommonBundle extends Bundle
{
}
