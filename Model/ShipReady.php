<?php
 /**
 * Created by Rubikin Team.
 * ========================
 * Date: 10/20/2014
 * Time: 1:38 AM
 * Author: vu
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Model;


use Nilead\ProductComponent\Model\VariantInterface;
use Nilead\SalesComponent\Model\OrderItemInterface;
use Nilead\ShipmentCommonComponent\Model\ShipReadyInterface;
use Nilead\ShipmentCommonComponent\Model\Traits\ShippableTrait;

class ShipReady implements ShipReadyInterface
{
    use ShippableTrait;

    /**
     * @var OrderItemInterface
     */
    protected $orderItem;

    /**
     * @var int
     */
    protected $quantity;

    /**
     * {@inheritdoc}
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * {@inheritdoc}
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
