<?php
/**
 * Created by Rubikin Team.
 * Date: 4/25/14
 * Time: 2:58 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentCommonBundle\Tests\unit;

use \Mockery as m;

trait TraitPackageTest
{

    public function generatePackage($addresses)
    {
        $package = m::mock('Nilead\ShipmentBundle\Packaging\Package\Package')->makePartial();

        if (count($addresses)) {
            foreach ($addresses as $key => $address) {
                $package->shouldReceive('get' . ucfirst($key))
                    ->andReturn($this->generateAddress($address));
            }
        }

        $package->shouldReceive('getWeight')->andReturn(5);

        $package->shouldReceive('getQuantity')->andReturn(5);

        $methods = [
            'carrierCode' => [
                'methodCode1' => $this->generateShippingMethod('methodCode1'),
                'methodCode2' => $this->generateShippingMethod('methodCode2')
            ]
        ];

        $package->shouldReceive('getShippingMethodByCode')->andReturnUsing(
            function ($carrierCode, $methodCode) use ($methods) {
                return $methods[$carrierCode][$methodCode];
            }
        );

        $package->shouldReceive('getShippingMethods')->andReturn($methods);

        return $package;
    }

    public function generatePackageItem()
    {
        $item = m::mock('Nilead\ShipmentComponent\Model\ShipmentItemInterface');

        $orderiItem = m::mock('Nilead\SalesComponent\Model\OrderItemInterface');

        $shippable = m::mock('Nilead\ShipmentCommonComponent\Model\ShippableInterface');
        $shippable->shouldReceive('getName')->andReturn('some name');

        $item->shouldReceive(['getShippable' => $shippable, 'getQuantity' => 1, 'getOrderItem' => $orderiItem]);

        return $item;
    }

    public function generateAddress($address)
    {
        $shipToAddress = m::mock('Nilead\AddressBundle\Model\Address')->makePartial();

        foreach ($address as $key => $value) {
            $shipToAddress->shouldReceive('get' . ucfirst($key))
                ->andReturn($value);
        }

        return $shipToAddress;
    }

    public function generateShippingMethod($methodCode)
    {
        $shippingMethod = $shipToAddress = m::mock('Nilead\ShipmentBundle\Model\ShippingMethod');

        $shippingMethod->shouldReceive('getName')
            ->andReturn('methodName');

        $shippingMethod->shouldReceive('getCarrierCode')
            ->andReturn('carrierCode');

        $shippingMethod->shouldReceive('getMethodCode')->andReturn($methodCode);

        $shippingMethod->shouldReceive('getAdjusters')->andReturn(
            [
                $this->generateAdjuster(),
                $this->generateAdjuster()
            ]
        );

        return $shippingMethod;
    }

    public function generateAdjuster()
    {
        $shippingMethodAdjuster = $this->getMock('Nilead\ShipmentBundle\Model\ShippingMethodAdjuster');

        return $shippingMethodAdjuster;
    }
}
